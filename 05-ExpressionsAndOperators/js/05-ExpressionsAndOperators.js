// Example of a non expression:
// a;       // a? There is no variable named "a", no function named "a", there is nothing Javascript can do with this statement. Therefore it is not an expression

// Types of Expressions:

// 1-Variable Declaration

let a;          // Here "let" is an operator, it tells Javascript to do something, in this case declare a variables. 
let b;          // In the other hand "a" and "b" are operands. They tell Javascript to give those variables the names "a" and "b".
        
// 2-Assign Value

a = 4;          // "=" is an operator; "a", "b" and "4" are an operands
b = 4;

// 3- Perform an evaluation that returns a single value
// b + c        // This is an expression but the statement is still incomplete

// The folowing statement contains three expressions:

let c = a + b;  // 1. Variable declaration "c".
                // 2. Perform evaluation "a + b".
                // 3. Assign result to "c"

// Categories of Operators 

// Assignment =
// Arithmetic + - * / %    
// Increment/Decrement (could be considered as arithmetic as well) ++ --
// String '' + (+ = concatenate strings)
// Precedence ( )
// Logical && (and) || (or)
// Member accessor operator  .
//        example: the "." in console.log() indicates the acces to the "log" member of the "console" object
// Code block operators {}
// Array element access operators []     

// Notes: When using ++ or -- beware:

let d = 1;
d++;
console.log('Printing incremented values example:');
console.log(d++);   // One would think that the value "3" would be printed, but the printed value turns up to be "2"
console.log(d);     // Now the value "3" is printed. This is because in the previous line console.log() used "d" to print
                    // a message and then incremented the value. We can use the following line in order to print the "d + 1":
console.log(++d);   // This wil print "4" instead of "3" as inteded.

// % = Modulus (left over from a division) Example:

let m = 10 % 3; // 10/3 = 3 with 1 left over, m should be assigned "1".
console.log('Modulus Example:');
console.log(m); 

// Modulis (%) is usually requested in job interviews.

// Operations must be performed bewteen the correct data types:
let text1 = 'Hello', text2 = 'world';
console.log(text1 * text2); // There is no compiltation error but output of this line is NaN: Not a Number