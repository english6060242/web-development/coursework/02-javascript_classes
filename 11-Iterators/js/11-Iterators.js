// Print numbers from 0 to 10

// For Loop
for(let i = 0; i < 11; i++)
{
    console.log(`For loop: number ${i}`);
}

console.log(); // \n

// While
let num = 0;
/* let num = 11; */
/* while(num < 11) */
while(num <= 10) // Another way to do it is using <= and 10 instead of 11
{
    console.log(`while loop: number ${num}`);
    num++;
}

console.log(); // \n

// Print numbers from 0 to 50 and determine whether they're even or uneven numbers
// Do while
let num2 = 0;
/* let num2 = 51; */
do
{
    if(num2 % 2 === 0) // num2 % 2 will obtain the remainder of the division (4/2 = 2, remainder = 0 => even, 3/2 = 1, remainder = 1 != 0 => uneven)
    {
        console.log(`Do while loop: number ${num2} is EVEN`);
    }
    else
    {
        console.log(`Do while loop: number ${num2} is UNEVEN`);
    }
    num2++;
}while(num2 <= 50);

// The difference between a While and a do While is that the "do While" loop will exectute at least the first iteration even if the
// condition is false. On the other hand, the "while" loop will NOT execute any iterations if the condition is false from the begining.
// For example: if num 2 where 51, the result would be: "Do while loop: number 51 is UNEVEN". However, if num where 11, the while loop
// would not print anything. This is a question that usually appears on job interviews.

console.log(); // \n

// Iterators are particularly useful for array management.
const shopcart = [
    { name: 'Smartphone', price:700},
    { name: 'Laptop', price:1300},
    { name: 'Headset', price: 150}
];

let total = 0;
for(let i = 0; i < shopcart.length; i++)
{
    console.log(`Item 1: ${shopcart[i].name}, price: ${shopcart[i].price}`);
    total += shopcart[i].price;
}
console.log(`Total: ${total}`); 

console.log(); // \n
total = 0;

// However, the following iterators are the most commonly used when it comes to working with arrays in JavaScript.
// For each
shopcart.forEach(product => 
    {
        console.log(`For each: Item 1 : ${product.name}, price ${product.price}`); 
        total += product.price;
    });
console.log(`For each: Total: ${total}`); 

console.log(); // \n
total = 0;

// map
shopcart.map(product => 
    {
        console.log(`map: Item 1 : ${product.name}, price ${product.price}`); 
        total += product.price;
    });
console.log(`map: Total: ${total}`); 

console.log(); // \n

// Both forEach and map have the same syntax; however, they are used for different purposes.
// Use forEach when you want to perform actions without assigning values.
// On the other hand, map is the preferred choice when you need to create a new array with transformed values. For example:

// This will work:
let newShopcart = shopcart.map(product => product.name); // Creates a new array containing only the names of the products
console.log(newShopcart);

console.log(); // \n

// This will not work as expected (result will be undefined):
let newShopcart2 = shopcart.forEach(product => product.name); // Performs an action on each element but does not create a new array
console.log(newShopcart2);

// The difference between map and forEach is also commonly asked in job interviews.