// Variables itself do not have data types but the the values we can store in them do.

let x = 4;              // This type is "number". It can store possitive and negative velues as well as decimal numbers.

// Once you have determined the data type of a variable you may not change it. 
//let x = 'Hello';      // Uncommenting this statement will produce an error.

console.log('x type: ' + typeof x);

let y1 = 'Hello';               // This type is "string"
let y2 = String('Hello');       // Stirng variables may also be initialized using the String() constructor.
let y3 = new String('Hello');   // Stirng variables may also be defined as an object, using the new String() constructor.
let y4 = 'Time 2"1\'';          // In order to use " or ' inside the text, the quotation marks must be nullified using \.
let z1 = true;                  // This type is "boolean"
let z2 = new Boolean(true);     // Booleans can also be declared as an object via constructor.

console.log('y1 type: ' + typeof y1);
console.log('y2 type: ' + typeof y2);
console.log('y3 type: ' + typeof y3);
console.log(y4);
console.log('z1 type: ' + typeof z1);
console.log('z2 type: ' + typeof z2);

let u;                  // The type and value of this variable is undefined

console.log('u type: ' + typeof u);

// Strings and numbers are displayed in different colors in the browser's console.