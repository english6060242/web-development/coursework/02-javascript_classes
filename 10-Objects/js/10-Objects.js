"use strict"
// ----- Objects -----
let myObject = {
    name : 'my name',
    age: 28,
    height: 1.80
}

// Access the object's properties:
console.log(myObject);
console.log(myObject.name);
console.log(myObject.age);
console.log(myObject.height);

// Another sintax:
console.log(myObject['name']);
console.log(myObject['age']);
console.log(myObject['height']);

// You may also add/initialize new properties and delete existing properties:
myObject.gender = 'Male';
delete myObject.height;

console.log(myObject);

// An object's properties do NOT count as js variables. Therefore, an object's poperty and a variable can share the same name without conflict:
/* let name = myObject.name;

console.log(name); */ // Uncomment this code and comment the Object Destructuring section to see results.

// Object Destructuring: This is a different sintax to extract property values from objects. Unlike the variable = Object.Property sintax,
// object structuring allows multiple property values to be extracted in one line:
const {name, age, gender} = myObject;

console.log(name);

// Strict coding: An object declared as "const" instead of "let" will still be able to be modified and have properties added/deleted.
// This is a bit of a contradiction since "const" implies that a variable cannot be modified. In order to prevent a const object from
// being modified, you may use the freeze() function. However, the JavaScript code must be run in strict mode for this to take effect. This can
// be achieved by writing "use strict" at the beginning of the code.

const myObject2 = {
    id : 1,
    price : 50,
    color : 'red'
} 

Object.freeze(myObject2);

/* myObject2.year = 2022; */    // Because freeze has been called, this will take no effect and if "use strict" has been declared, then there
                                // will be an error when compiling.

console.log(myObject2);

// You may also check whether an object can be modified or not (in case you're using objects from external code that you havent written yourself)
// using isFrozen().

console.log('Is myObject frozen? '+Object.isFrozen(myObject));
console.log('Is myObject2 frozen? '+Object.isFrozen(myObject2));

// Freezing an object will prevent you from adding and deleting properties but it will also prevent you from modifying those poperties.
// A less restrictive alternative is seal(). Sealing an object will prevent you from adding and deleting properties but it will NOT prevent 
// you from modifying those poperties.

const myObject3 = {
    size : 'Large',
    color : 'brown',
    available : true
}

Object.seal(myObject3);

// This will have no effect and generate an error in strict mode:
/* myObject3.releaseDate = 2023; 
delete myObject3.color; */

// This will take effect and generate no errors.
myObject3.available = false;

console.log(myObject3);

// Join Objects via "Spread Operators or Rest Operators"
const objectA = {
    productName : '40" Screen',
    price : 300,
    available : true
};

const objectB = {
    weight : '8 Kg',
    color : 'black'
};

// Joining objects using this method is highly recommended since it's good practice NOT to modify the original objects.
const joinedObject = { ...objectA, ...objectB}; 

console.log(joinedObject);

// ----- Property Methods -----
const mediaPlayer = {
    play : function(id) {
        console.log(`Playing song id: ${id}`); // Remember to use `` instead of '' when a variable is involved
    },
    pause : function() {
        console.log('Pausing...');
    },
    createPlalist : function(name) {
        console.log(`Creating PLaylist ${name}`);
    }
};

// Property Methods can also be added the same way regular properties are added to objects
mediaPlayer.deleteSong = function(id) {
    console.log(`Deleting song nº ${id}`);
};

console.log('\nUse mediaPLayer\'s property method: play\n');
mediaPlayer.play(2);
mediaPlayer.pause();
mediaPlayer.deleteSong(20);
mediaPlayer.createPlalist('Training Music');

// Object's properties can also be funcions'

let object = {
    name: 'myObject',
    messege: function(){console.log('\nHello, I am an object\n');}
}

// Remember to call the function in order for it to take effect.
object.messege();

// Functions within objects can also reference the object's properties using "this.poperty".
const person = {
    name: 'John',
    age: 30,
  
// Define a function 'greet' as a property of the object.
// This function uses 'this' to access the object's properties 'name' and 'age'.
greet: function() {
    console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
},

    /* Now, let's define another function using an arrow function.
    Note that arrow functions do not have their own 'this' binding,
    so they cannot access the object's properties directly.
    Attempting to use 'this' within an arrow function will reference the 'window' object instead of 'person'. */
   /*  greetArrow: () => {
    console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`); 
    }*/
};
  
// Call the 'greet' function to display a message.
person.greet();
  
// The result of an arrow function such as greetArrow() within an object is commonly asked in job interviews.
// The result of uncommenting the following line would be: "Hello, my name is undefined and I am undefined years old."
/* person.greetArrow(); */
