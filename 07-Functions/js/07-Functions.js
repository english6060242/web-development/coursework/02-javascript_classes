// Function declaration:
function myFunction() // Function names adhere to the same naming rules as variables.
{
    console.log('My Function');
}

function myFunction1()
{
    console.log('I am function 1');
}

function myFunction2()
{
    console.log('I am function 2');
}

function myFunction3()
{
    console.log('I am function 3');
}

// Calling the function by it's name
console.log('\n-----Call myFunction-----\n');
myFunction();

// Assign the function to a variable. This way you get a reference to the function. The variable is "pointing" to the funcion
let a = myFunction;
console.log('\n-----Call myFunction via variable "a"-----\n');
a();

console.log('\n-----Use an array of functions in order to call functions by index-----\n');
let b = [myFunction1, myFunction2, myFunction3];
for (let i = 0; i < 3; i++)
{
    b[i]();
}

// Passing arguments
// Function for "Hello World" program:
function helloWorld(name)
{
    console.log('Hello ' + name + '!');
    console.log('\nArgument type:  ' + typeof name + '\n');
}

console.log('\n-----Function for "Hello World"-----\n');
// Type of "name" will be the type of the argument used when calling the function. 
// The same function may be called with several data types.
helloWorld('Lucas');    
helloWorld(150);
helloWorld(true);

// Functions with return values
function myAdder(a, b)
{
    console.log('myAdder was called with the arguments ' + a + ' and ' + b);
    return a + b;
}

console.log('\n-----myAdder function-----\n');
let c = myAdder(1,2);
console.log('\nresult = ' + c);

// Functions can be declared with default parameters in case one or more parameters are not specified
// when invoking the function:
function myMultiplier(num1 = 1, num2 = 2) 
{
    console.log(num1 * num2);
}

console.log('\n----- Function with default parameters -----');
console.log('\nmyMultiplier(3,4) = ');
myMultiplier(3,4);
console.log('\nmyMultiplier(5) = ');
myMultiplier(5);

// JavaScript handles hoisting for function declarations and for function expressions defferently.
// Function declarations are hoisted in their entirety, including their name and code, allowing them 
// to be called before their declaration in the code:
console.log('\n-----myAdder2 function-----');
myAdder2();
function myAdder2 () {
    console.log(10 + 10);
}

// On the other hand, Function expressions, which are assignments to variables, have their variable 
// declarations hoisted but not their assignments, so they must be declared and assigned before they 
// can be called:
// Uncommenting the following code will generate an error.
/* console.log('\n-----myAdder3 function-----');
myAdder3();
let myAdder3 = function () {
    console.log(10 + 10);
} */

// Knowing the difference between function declarations (function myFunction(){}) and for function expressions
// (let f1 = function(){}) is often required in job interviews.

// ----- Functions vs Methods -----
const num1 = 20;
const num2 = '20';

console.log(parseInt(num2));    // parseInt() is a function
console.log(num1.toString());   // toString() is a method

// ----- Arrow Functions -----
// From this sintax
/* const myAdder4 = function (n1, n2){
    console.log('\nresult = '+n1 + n2);
};*/

// To this sintax:
const myAdder4 = (n1, n2) => console.log(n1 + n2); // If there is only one line in the function, the {} after => are optional

console.log('\n-----myAdder2 function-----');
myAdder4(5,10);

const myArrowFunction = (myString) => {         // Also, when the function uses a single argument, the () that enclose the argument are optional.
    console.log(`\n-----myArrowFunction-----\n${myString}`);
}

myArrowFunction('Hello World');

// Arrow Functions are useful for array methods
// Method C: Using some. If an array consists on multiple object elements, includes will not work.
// Use this method instead:
const objectArray = [
    {name : 'John',  grade : 9 },
    {name : 'Gwen',  grade : 10},
    {name : 'James', grade : 7 },
    {name : 'Peter', grade : 10}
];

/* let result2 = objectArray.some(function(obj){ // Before
    return obj.name === 'James';
}); */

// After
// Only one argument so () is optional / only one line so {} is optional and return is implicit
let result2 = objectArray.some(obj => obj.name === 'James'); 

console.log('\n-----Array Method via Arrow Funtion-----');
console.log('\n'+result2);

// Another example:
// Before
/* let auxSum = objectArray.reduce(function(total,obj){
    return total + obj.grade;
}, 0); */

// After
/* let auxSum = objectArray.reduce((total,obj) => total + obj.grade, 0); */