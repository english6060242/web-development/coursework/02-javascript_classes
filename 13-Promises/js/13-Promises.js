// Create a new Promise called userAuth, which takes a function with two arguments: resolve and reject.
const userAuth = new Promise ((resolve, reject) => 
{
    const auth = false;

    
    if(auth) { resolve('Access Granted'); }  // Check if 'auth' is true, if so, resolve the promise with 'Access Granted'.
    else { reject('Unable to login'); } // If 'auth' is false, reject the promise with 'Unable to login'.
});

//console.log(userAuth); // Promise{Fullfilled, 'Acces Granted'} or an error and Promise{Rejected, 'Unable to login'}

// Promises are typically used for asynchronous operations. You can attach
// a 'then' method to handle the resolved value and a 'catch' method to handle errors.

// Promises may take three different values:
// Pending: The promise hasn't been resolved nor rejected. If there is no code inside the promise, this will be the resulting value.
// Fullfilled: The promise has been resolved.
// Rejected: The promise has not been fullfilled.

userAuth
    .then(function(result) {console.log(result);}) // This code block is executed when the promise is resolved (auth is true).
    .catch (function(error) {console.log(error);}) // (executed when the promise is rejected). This is important, without this the program wil result in an error  if the peomise is rejected.
