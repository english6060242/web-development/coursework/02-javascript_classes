// The Fetch API is a built-in JavaScript feature that allows you to make network requests (e.g., HTTP) to retrieve data 
// from remote servers or send data to them. It provides a more modern and flexible way to handle asynchronous data fetching
// and is commonly used for making AJAX requests. With Fetch, you can easily fetch data, such as JSON, from web services and 
// handle the response in a more efficient and readable manner. It's a fundamental tool for working with external data sources 
// in web development.

function getEmployees() {
    const myFile = 'employees.json'; // Ensure the file path is correct; otherwise, it will result in an HTTP 404 error.
    
    fetch(myFile)
        .then(response => {
            /* console.log(response); */ // This will show us information about the request (ej success or error http code, data format, ect) but not the content itself.
            return response.json(); // This line parses the response as JSON, converting it to a JavaScript object. 
        })
        .then(data => {
            // This code will execute only if the data is in JSON format and if the connection to the server (local file in this case) was successful. 
            /* console.log(data);  */ // Print all data
            const { employees } = data;         // Destructuring: creates an array using the JSON file's data (contains all elements)
            employees.forEach(employee => {     // 'employee' represents an individual element from the JSON file 
                /* console.log(employee); */ 
                // We can use "employee" to access the fields of an element 
                console.log(employee.id);       
                console.log(employee.name);
                console.log(employee.position);
            });
        })
        .catch(error => {
            console.error(error); // In case of any errors during the fetch or JSON parsing, this line will handle and log the error.
        });
}

/* getEmployees(); */

// Since we are accessing a database, which may involve waiting for data, the function that contains the fetch must be declared as asynchronous (async).

async function getEmployees2() {
    const myFile = 'employees.json'; 

    // Also, we can use the 'await' instead of explicit promises, simplifying the syntax.
    const result = await fetch(myFile);
    const data = await result.json(); // This line also uses 'await' because it depends on the success of the previous line. 
                                      // It will only be executed after the fetch, similar to a promise's behavior. 
    console.log(data);   
}

getEmployees2();