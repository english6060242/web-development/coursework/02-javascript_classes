// Object-Oriented Programming in JavaScript

// Object Literal
const myObject = {
    objName: 'myObject1',
    objValue: 50
};

console.log(myObject);
console.log();

// Previous JavaScript syntax for an Object Constructor (Note: You might want to avoid using this outdated syntax)
function Fruitpack(name, quantity) {
    // The Fruitpack function serves as a class. You can create multiple instances (objects) with different values.
    this.name = name;
    this.quantity = quantity;
}

const firstObject = new Fruitpack('Apples', 4);
const secondObject = new Fruitpack('Oranges', 7);

console.log(firstObject);
console.log();
console.log(secondObject);

console.log();

// ----- Prototypes -----
// In this example, the printTool function is used to print the name and price of a Tool object.
function Tool(name, price) { // Tool class
    this.name = name;
    this.price = price;
}

function printTool(tool) { console.log(`This tool's name is ${tool.name} and it costs $${tool.price}`);}

const myTool = new Tool('Hammer', 43); // Tool object

printTool(myTool);
console.log();

// However, calling printTool using a Fruitpack (or any other object) will not result in an error.
// Instead, when printTool attempts to access the "price" property, it will find an undefined value.

printTool(firstObject); // This tool's name is Apples and it costs $undefined
console.log();

// Prototypes can be used to avoid this undesired behavior.
// Prototypes are a type of function that can only be used for objects of the class they were created for.

Tool.prototype.printToolExclusive = function() { // Use lowercase for method names
    console.log(`This tool's name is ${this.name} and it costs $${this.price}`);
}

myTool.printToolExclusive();
console.log();
/* firstObject.printToolExclusive(); // Uncommenting this will result in an error */

// Up to this point the code has been using ways to emulate Object Oriented Programming (this is how this used to be done in JavaScript).
// However the classes created using functions are not real classes. Proper classes where included for this language in 2015.

// ----- Classes -----
class Cake // By convention, classes' names start with uppercase.
{
    constructor(name, flavor, price) // Unlike Java, where the constructor method shares the name of the class, in JavaScript there 
    {                                // is a reserved function name callled 'Constructor' for this end.
        this.name = name;            // Also, by writing the constructor the properties are also declared, ther is no need to declare
        this.flavor = flavor;        // every property like in Java.
        this.price = price;
    }

    printCake() // Method
    {
        console.log(`This cake is called ${this.name}, it's flavor is ${this.flavor} and it's price is $${this.price}\n`);
    }

    // Getter methods
    name() {return this.name;}
    flavor() {return this.name;}
    price() {return this.name;}
}

let first_cake = new Cake('Lemon Pie', 'Lemon', 30);
let second_cake = new Cake('Cabsha', 'Chocolate', 25);

first_cake.printCake();
second_cake.printCake();

// Inheritance
// If there are many classes whose objects share properies, repeating those fields in each constructor will result
// in an unnecesary large code. However, classes may inherit the constructor/poperties and methos of another class.
// This is shown in the following example, where multiple types of products are created for an online shop.
class Product 
{
    constructor(name, price)
    {
        this.name = name;
        this.price = price;
    }

    // Getter Methods
    name() {return this.name;}
    price() {return this.name;}

    // Setter Methods
    setName(name) {this.name = name}
    setPrice(price) {this.name = price}

    // Printing method
    printProduct() { return `The product '${this.name}' costs $${this.price}`;}
}

class T_Shirt extends Product // This indicates that the T_Shirt class inherits from the Product class.
{
    constructor(name, price, size, color)
    {
        // this.name = name; // Instead of repeating this assingments,
        super(name, price); // super() is used to get what is to be done with the name and price properties from the Product class.
        this.size = size;   // Theese properties are new (not included in the Product class), so they must be initialized.
        this.color = color; 
    }

    // Getter Methods
    size() { return this.size; }
    color() { return this.color; }

    // Setter Methods
    setSize(size) { this.size = size;}
    setColor(color) { this.color = sizcolore;}

    // Inherited methods can be overwritten in order to better suit the heir class
    printProduct() { return `${super.printProduct()}, it's color is ${tshirt_1.color} and it's size is ${tshirt_1.size}\n`}
    // ${tshirt_1.color} can be used to add text to Product's print method but the method can also be changed entirely
}

let tshirt_1 = new T_Shirt('Cool Red T-Shirt', 5, 'XL', 'Red');

console.log(tshirt_1.printProduct());

// ---- try / catch ----
// In JavaScript, the try...catch statement is used to handle exceptions (runtime errors) that might occur.
// Example:
try {
    // Code that might throw an exception
    let result = 10 / 0; // This will throw a division by zero exception. If this was not surrounded by try - catch, the porgram would reuslt in an error.
    console.log(result); // This line won't be executed
  } catch (error) {
    // Code to handle the exception
    console.error("An error occurred: " + error.message); // Prints the error message
  } finally {
    // Code that will always run, whether an exception occurred or not
    console.log("This will always be executed.");
  }
  
  console.log("Program continues..."); // This line will be executed after the try-catch block

// There are several actions that should be surrounded by try - catch, since they might produce an exception. Here are some of them:
// Accessing Undefined Variables: Trying to access a variable that has not been declared or is out of scope will result in a ReferenceError.
// console.log(undefinedVariable); // ReferenceError

// Accessing Undefined Properties: Attempting to access a property of an undefined or null object will generate a TypeError.
// let obj = null;
// console.log(obj.property); // TypeError

// Dividing by Zero: Dividing a number by zero will throw a TypeError.
// let result = 10 / 0; // TypeError

// Parsing Invalid JSON: Trying to parse invalid JSON using JSON.parse will raise a SyntaxError.
// JSON.parse("invalid JSON"); // SyntaxError

// Calling Non-Existent Functions: Calling a function that doesn't exist or hasn't been defined will throw a TypeError
// nonExistentFunction(); // TypeError

// Using Incorrect Data Types: Using data types incorrectly, like trying to invoke a non-function as a function, will result in a TypeError.
// let num = 42;
// num(); // TypeError

// Handling Asynchronous Code Errors: When dealing with asynchronous code, errors can be thrown during Promise rejection. You can catch these errors using .catch() or a try...catch block.
/* fetch("nonExistentURL")
  .then(response => response.json())
  .catch(error => console.error(error)); */ // Handles network errors

// File I/O Errors: When working with file I/O in environments like Node.js, file operations can throw exceptions if files don't exist or permissions are insufficient.
/* const fs = require("fs");
try {
  const data = fs.readFileSync("nonExistentFile.txt", "utf-8");
} catch (error) {
  console.error(error); // Handles file read errors
} */

// Regular Expression Errors: Invalid regular expressions can cause errors, such as SyntaxError.
/* try {
    new RegExp("["); // SyntaxError
  } catch (error) {
    console.error(error);
  } */
  
// Index Out of Bounds: Accessing an array element with an out-of-bounds index will not throw an exception, but it will return undefined. However, you can manually check array bounds or handle this situation in your code.
/* const arr = [1, 2, 3];
console.log(arr[10]); */ // undefined (no exception)

// Type Coercion Issues: Type coercion in JavaScript can sometimes lead to unexpected results or exceptions. For example, adding a string and a number may not always behave as expected.
//console.log("5" + 5); // "55" (string concatenation)
// console.log("5" - 5); // 0 (numeric subtraction)