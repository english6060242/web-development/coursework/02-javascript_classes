// Select the button element with the id 'myButton'
const button = document.querySelector('#myButton');

// Add a click event listener to the button and log a message when clicked
button.addEventListener('click', function () {
    console.log('You clicked');
});

// Add another click event listener to the button to request permission for notifications
button.addEventListener('click', () => {
    // The Notification API uses promises to handle permission requests.
    // We use .then() to handle the result when the user makes a choice.
    Notification.requestPermission()
        .then(result => console.log(`The result is ${result}`));
});

// Check if notification permission has already been granted
if (Notification.permission == 'granted') {
    // Create a new notification if permission is granted
    // You can provide additional options in the second argument of new Notification()
    new Notification('Hi, this is a notification!', {
        icon: 'img/icon.png', // You can specify an icon for the notification (relative or absolute URL)
        body: 'Body goes here.' // You can specify the notification message body
        // You can add more options here like 'image', 'badge', 'actions', etc.
    });
}
