// Declearing arrays
let a = [4, 8, 15, 16, 23, 42];
let b = ['Lucas', 'David', 'Alice'];

// Printing arrays and array items
console.log('-----Print current values of an array-----\n');
console.log('a[0] = ' + a[0]);  // This will print "4"
console.log('a[1] = ' + a[1]);  // This will print "8"
console.log('Array "a":'); 
console.log(a);     // This will print "[4, 8, 15, 16, 23, 42]"

//  Modifying array items: arrayName[itemIndex] = newValue
console.log('\n-----Modifying array items-----');
a[0] = 7;

console.log('\nModified value a[0] = ' + a[0] + '\n');

// Arrays' type is object
console.log('-----Type of an array-----');
console.log('\nType of "a" = ' + typeof a);
console.log('Type of "b" = ' + typeof b+ '\n');

// Arrays may contain different data types (strings, numbers, other arrays, objects, null, booleans, etc)
console.log('-----Elements of different types-----\n');
let c = [1, "Lucas", true];
console.log('Array "c":'); 
console.log(c); 

// Just like a variable without an assigned value, if an element of an 
// array does not exists (it has not been delcared), it will be undefined:
console.log('\n-----Undefined array items-----');
console.log('\nc[3] = ' + c[3]); // c contains c[0], c[1] and c[2] => c[3] will be undefined

// Determine the number of elements in an array:
console.log('\n-----Array length-----');
console.log('\nThe array "a" has ' + a.length + ' elements');

// Arrays may have empty elements
console.log('\n-----Empty elements-----');
a[10] = 77;
console.log('\nArray "a":'); 
console.log(a)
console.log('\nThe array "a" has ' + a.length + ' elements');

// Creating elements with nothing in them is not safe. Use the push built-in function to 
// add elements at the end of the array regardless of the number of existing elements in the array
console.log('\n-----Using push() function-----');
b.push('James');
console.log('\nArray "b":'); 
console.log(b)
console.log('The array "b" has ' + b.length + ' elements');

// You may also remove the last item of the array using the pop built-in function
console.log('\n-----Using pop() function-----');
a.pop();
a.pop();
a.pop();
a.pop();
a.pop();
console.log('\nArray "a":'); 
console.log(a)
console.log('The array "a" has ' + a.length + ' elements');

// The "unshift" function can be used to add elements at the beginning of the array:
b.unshift('John','Tyler','Stacy');
console.log('\n-----Adding elements at the beginning of the array:-----');
console.log('\nArray b = ',b);

// The "shift" function can be used to remove the first element of the array:
b.shift();
b.shift();
b.shift();
console.log('\n-----Removing elements at the beginning of the array:-----');
console.log('\nArray b = ',b);

// If you want to remove an element that is not at the begining nor at the end of the array,
// you may use the splice function:
let temperatures = ['36º', '27º', '30º', '32º', '22º'];
temperatures.splice(2,1); // Sintax: splice(index of the element you want to remove, amount of elements to be removed from that index)
console.log('\n-----Removing elements a single or multiple element in any position of the array:-----');
console.log('\nArray temperatures = ',temperatures);

// push, pop, unshift, shift and splice are often requiered in job interviews.
// However, the tendency seems to be NOT modifying the original arrays but creating new ones instead.
// Rest Operators and Spread Operators can be use to that end.
let newTemperatures = [...temperatures,'25º','33º'];
let newTemperatures2 = ['25º','33º',...temperatures];
console.log('\n-----Adding elements to a new array (at the end), copying the previos one first:-----');
console.log('\nArray newTemperatures = ',newTemperatures);
console.log('\n-----Adding elements to a new array (at the beginning), copying the previos one first:-----');
console.log('\nArray newTemperatures2 = ',newTemperatures2);

// Arrays may also be defined via constructor
const months = new Array('January','february','March','...');
console.log('\n-----Constructor-declared Arrays-----');
console.log('\n Array months = ',months);

// Array iterators
console.log('\n-----Print Array Via Iterator-----\n');
months.forEach(function(month){console.log(month);}); // "month" will serve as i; array[i]

// Find out whether an element is contained in the Array
// Method A: Using forEach
const days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

console.log('\n-----(Method A: forEach)Is Tuesday included in the days array?-----');
days.forEach(function(day){
    if(day == 'Tuesday')
    {
        console.log('\nTuesday is included in the array');
    }
});

// Method B: Using includes
console.log('\n-----(Method B: includes)Is Monday included in the days array?-----');
const result = days.includes('Monday');
console.log('\nResult = ',result);

// Method C: Using some. If an array consists on multiple object elements, includes will not work.
// Use this method instead:
const objectArray = [
    {name : 'John',  grade : 9 },
    {name : 'Gwen',  grade : 10},
    {name : 'James', grade : 7 },
    {name : 'Peter', grade : 10}
];

let result2 = objectArray.some(function(obj){
    return obj.name === 'James';
});

console.log('\n-----(Method C: some) - Is James included in the objectArray?-----');
console.log('\n result2 = ', result2);

// This can also be achieved using a shorter sintax:
result2 = objectArray.some(obj => obj.name === 'Gwen');

console.log('\n-----(Shorter sintax for method C: some) - Is Gwen included in the objectArray?-----');
console.log('\n result2 = ', result2);

// More array methods: reduce()
// The reduce() method takes two parameters:
    // 1. A callback function that is executed for each element in the array.
    //    - The 'total' parameter represents the accumulated result.
    //    - The 'obj' parameter represents the current element being processed (an object in this case).
    // In each iteration, add the 'grade' property of the current object to the 'total'.
    // 2. An initial value for the 'total' parameter, in this case, it's initialized to 0.
let auxSum = objectArray.reduce(function(total,obj){
    return total + obj.grade;
}, 0);

// The 'auxSum' variable now contains the total sum of all student grades.

// What is the average student grade?
let avg = auxSum / objectArray.length;

console.log('\n-----reduce() - What is the average student grade?-----');
console.log('\n Average Grade = ', avg);

// More array methods: filter
// The filter array method allows us to select only the elements that meet certain criteria
let aPlusStudents = objectArray.filter(function(obj){
    return obj.grade == 10;
});

console.log('\n-----filter() - Which students achieved maximum grade?-----');
console.log('\n A+ students = ', aPlusStudents);

// Filter can also be used to obtain elements with cartain name or any other criteria over any of
// the other properties of the array's objects.