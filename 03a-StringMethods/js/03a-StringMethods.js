let text = 'Hello, please count the amount of charcartes in this line';

// length
console.log(text.length);

// IndexOf
console.log(text.indexOf('please')); // IndexOd will find the position of a substring or character within the complete string.
console.log(text.indexOf('red')); // If indexOf returns -1 => the charcacter or substring does not exist within the complete string.

// Includes
console.log(text.includes('amount')); // Includes returns true if the char/string is contained in the complete string,
console.log(text.includes('red'));    // or false if it isn't.

// Concatenate
let text1 = 'Hello', text2 = 'world';
console.log(text1 + ' ' + text2);

// Template Strings: Another way to concatenate
console.log(`It reads: ${text1} ${text2}`); // ${variable or function}

