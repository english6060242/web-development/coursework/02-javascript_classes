// Async / await

/* async/await in JavaScript is a feature used to work with asynchronous operations in a more readable and sequential manner.

For example, consider a scenario where you have a short task (e.g., updating a user's profile picture) and
a long task (e.g., fetching data from a remote server). You can use async/await to ensure that the short task
doesn't have to wait for the long one.

By marking a function as async, you indicate that it contains asynchronous operations. The await keyword is used
inside this function to pause its execution until a Promise (representing an asynchronous task) is resolved. While
waiting for the long task, other parts of your code can continue executing, ensuring that the short task doesn't 
block the overall progress of your program. This keeps your application responsive and efficient. */

// In order to simulate this example:
// setTimeout executes a function after the desired amount of time in ms.
// setTimeout (function() {console.log('setTimeout...')},1000); // Print 'setTimeout...' after one second (1000ms)
// setInterval(f,t) will execute the provided f function every t ms.
// setInterval (function() {console.log('setInterval...')},1000);

function downloadNewClients() // This function will download client data from a server, which will take some time.
{
    return new Promise(resolve =>                                           // For the example, only resolving the promise will be possible.
    {                                                                   // However, keep in mind that the promise could also be rejected when accessing a server.
        console.log('Downloading client data...');
        setTimeout(() => {resolve('Client data downloaded')}, 5000); // Supose this function needs five seconds to finish.
    })
}

function downloadLastOrders() // This function will download  data from a server, which will take some time.
{
    return new Promise(resolve =>                                           // For the example, only resolving the promise will be possible.
    {                                                                   // However, keep in mind that the promise could also be rejected when accessing a server.
        console.log('Downloading Orders data...');
        setTimeout(() => {resolve('Orders data downloaded')}, 3000); // Supose this function needs three seconds to finish.
    })
}

async function app() { // Using async here allows the program to continue executing without waiting for "app" to complete.
    try {
        // Because getting data from a server can fail, a try-catch is needed.
        //const clients = await downloadNewClients(); // Using await here ensures that downloadNewClients' execution must finish before
        //console.log(clients);                       // executing the next line.
        //console.log('This print has to wait for the promise to resolve.'); // This print will wait until the promise is resolved.
        // It's not advisable to wait for independent tasks within the same async function. 
        // In this case, both 'downloadNewClients' and 'downloadLastOrders' are awaited sequentially, which means that the second 'await' 
        // will only execute after the first one has completed.
        // Uncommenting the following line contradicts the previous advice:
        // const orders = await downloadLastOrders();
        // Instead do this:
        const result = await Promise.all([downloadNewClients(),downloadLastOrders()]); // This way both downloadNewClients and downloasLastOrders
        // will be executed at the same time. Now app takes five seconds to complete both functions instead of eight.
        console.log(result[0]);
        console.log(result[1]);
    } catch (error) {
        console.log(error);
    }
}

app();
console.log('This print does not need to wait for the server request to be fulfilled/rejected'); // Therefore this will be printed first.
