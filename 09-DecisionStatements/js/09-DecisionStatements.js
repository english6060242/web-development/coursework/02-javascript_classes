// if statement

var count = 3;

console.log('----If statement example results-----\n');

if(count == 4)
{
    console.log("Count is 4");
}
else
{
    console.log("Count is not 4");
}
if(count > 4)
{
    console.log("Count is greater than 3");
}
else if(count < 4)
{
    console.log("Count is less than 4");
}
if(count == 3)
{
    console.log("Count is 3");
}

// Strict Conditional:
let count2 = 1000;
if( count2 === 1000)
{
    console.log("Count is 1000");
}

let count3 = "1000";
if( count3 === 1000)
{
    console.log("Count is 1000 (number)");
}
if( count3 == 1000)
{
    console.log("Count is 1000 (number or string)");
}

// Switch statement

console.log('\n----Switch statement example results-----\n');

//let hero = 'superman';
let hero = 'Batman';

console.log('Current superhero: ' + hero + '\n');

//switch(hero)
switch(hero.toLowerCase())  // You may use the "toLowerCase()" function so that the decision statement does not distinguish between lower an upper case
{
    case 'superman':
        console.log('Super Strength');
        console.log('X-ray Vision');  
        break;       // With out this break statement, for switch(element), if element matches the first case, it will also match
    case 'batman':   // the second and all following cases. If "element" matches the second case, it will also match the third and so on.
        console.log('Wealth');
        console.log('Fighting skills');
        break;       // You may or may not want this break statement. For example, Batman is a JLA member so the break statement could be omited
    default:
        console.log('JLA member');
}

// Ternary Operator

console.log('\n----Ternary Operator-----\n');
let a = 1, b = '1';
let myName = 'Lucas'
let result = (a == b) ? 'equal' : 'inequal'; // Result will be assigned 'equal' if a == b, otherwise it will be assigned 'inequal'
console.log('result = ' + result);  // In this case result = 'equal', notice that it does not matter that a = 1 and b = '1' (string representation of 1)
let result2 = (a === b) ? 'Strictly equal' : 'Not strictly equal';   // In orther to not consider 1 and '1' as equal you may use stric equality: ===
console.log('strict result = ' + result2);  // Strict equality does not allow Javascript's cohersion mecanism to convert data types so that they are the same.
console.log('Is my name Lucas? ' + ((myName == 'Lucas') ? 'Yes' : 'No'));  // You may also perform a decision without having to create a variable
                           // This (                                   ) allows 'Is my name Lucas? ' to be printed