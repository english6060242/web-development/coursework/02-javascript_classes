// Sometimes we need a function that will be called only once in the application. 
// That's what Function Expressions are for (similar to lambdas in functional programming)
// Function Expressions: function (arg1, arg2, ..., argN) {line1; line2; ...; lineM;}

// You may write and call a function expression and invoke it immediately by surrounding it 
// with (functional expression) and adding an (); 
// Theese are calles IIFE

(function(){console.log('Immediately Invoked Function Expression');})();

// IIFE stands for "Immediately Invoked Function Expression" and this functions are useful to
// protect variables. To understand this, imagine that two separate .js files are included
// in the same HTML usgin script elements, and they both have a variable named clientA. Because 
// they're both included in the HTML file, the actions of BOTH js scripts WILL TAKE EFFECT on
// BOTH clientA variables.
// To prevent this, clientA can be defined inside a IIFE (or a regluar function that is invoked once).

// For this example the setTimeout() function is utilized.
// setTimeout(function, timeout) waits for "timeout" miliseconds before executing "function"

setTimeout(function () {console.log('Waited 2 seconds before printing this message');} , 2000);

// The following example is a recursive function that increases a counter every time it is executed
// Scince there is no condition for the recursive function calling, the program will run indefenitely
// Use Ctrl-C to stop the execution

//console.log('\n----Third example----\n');

let counter = 0;

function timeout()
{
    setTimeout
    (
        function()  // First argument for setTimeout()
        {
            console.log('This function was called ' + counter++ + ' time/s' );
            timeout();
        } , 2000    // Second argument for setTimeout()
    );
}

timeout();

