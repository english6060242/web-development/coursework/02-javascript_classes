// Enter 'window' on the browser's console => see all Javascript functions
let a = Math.random(); 
let b = Math.random();
let c = Math.PI;
let d = Math.round(2.4); // Rounds up regularly
let e = Math.round(2.5); // Rounds up regularly
let f = Math.ceil(2.1);  // Rounds up always to the lower value
let g = Math.floor(2.7); // Rounds up always to the lower value
let h = Math.sqrt(16);   // Sqare root
let i = Math.abs(-200);  // Absolute Value
let j = Math.min(1, 2, 3, 4);
let k = Math.max(1, 2, 3, 4);
let l = Math.floor(Math.random() * 30); // generates a random whole number between 0 and 29 (inclusive).

console.log('a = ',a);
console.log('b = ',b);
console.log('c = ',c);
console.log('d = ',d);
console.log('e = ',e);
console.log('f = ',f);
console.log('g = ',g);
console.log('h = ',h);
console.log('i = ',i);
console.log('j = ',j);
console.log('k = ',k);
console.log('l = ',l);